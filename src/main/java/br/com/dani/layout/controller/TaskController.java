package br.com.dani.layout.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.dani.layout.model.Task;

@Controller
public class TaskController {

	List<Task> tasks = new ArrayList<>();

	@GetMapping(value = "/create")
	public String home() {
		return "create";
	}

	@PostMapping("/create")
	public String create(Task task) {
		Long id = tasks.size() + 1L;
		Task newTask = new Task(id, task.getName(), task.getDate());
		System.out.println("O nome da tarefa é " + task.getName());
		System.out.println(newTask);
		tasks.add(newTask);
		return "redirect:/list";
	}

	@GetMapping(value = "/list")
	public ModelAndView list() {
		ModelAndView mv = new ModelAndView("list");
		mv.addObject("tasks", tasks);
		return mv;
	}

	@GetMapping("/create/{id}")
	public ModelAndView edit(@PathVariable Long id) {
		ModelAndView mv = new ModelAndView("create");

		Task taskFind = tasks.stream().filter(task -> id.equals(task.getId())).findFirst().get();

		mv.addObject("task", taskFind);

		return mv;
	}
}
